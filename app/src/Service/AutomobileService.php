<?php

namespace App\Service;

use App\Entity\AutomobileModel;
use App\Repository\AutomobileModelRepository;

class AutomobileService
{
    private $automobileModelRepository;

    public function __construct(AutomobileModelRepository $automobileModelRepository)
    {
        $this->automobileModelRepository = $automobileModelRepository;
    }

    /*
    * @Return AutomobileModel
    */
    public function findNearestModel(array $automobileModels, string $partialString): ?AutomobileModel
    {
        // Initialized with element at index 0
        $result = $automobileModels[0];
        foreach ($automobileModels as $model) {
            // Comparison of label string with user input
            if (strtolower($model->getLabel()) == strtolower($partialString)) {
                $result = $model;
            }
        }
        return $result;
    }

    /*
    * @Return AutomobileModel
    */
    public function searchModel(string $modelString): ?AutomobileModel
    {
        $result = null;
        $models = [];

        // Initialized with full user input
        $partialString = $modelString;

        if (empty($models)) {
            // Separate all words in sentence
            $arrayString = explode(" ", $partialString);
            foreach ($arrayString as $key => $string) {
                // Find model with current word
                $partialString = $string;
                $models = $this->automobileModelRepository->findModelByLabelLike($partialString);
                // If no results 
                if (empty($models)) {
                    // Find model with current word and by removing spaces in database label column
                    $models = $this->automobileModelRepository->findModelByLabelLikeWithoutSpace($partialString);
                    if (!empty($models)) {
                        // Keep the models returned
                        break;
                    }
                } else {
                    // Keep the models returned
                    if (isset($arrayString[$key + 1])) {
                        // Look for a competitor model that has a second word
                        $partialStringCompetitor = $string . ' ' . $arrayString[$key + 1];
                        $modelsCompetitor = $this->automobileModelRepository->findModelByLabelLike($partialStringCompetitor);
                        if (!empty($modelsCompetitor)) {
                            // The competitors become selected models
                            $models = $modelsCompetitor;
                            $partialString = $partialStringCompetitor;
                        }
                    }
                    // Keep the models returned
                    break;
                }
            }
        }
        // Find the nearest model in selection
        if (!empty($models)) {
            $result = $this->findNearestModel($models, $partialString);
        }

        return $result;
    }
}
