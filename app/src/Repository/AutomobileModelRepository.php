<?php

namespace App\Repository;

use App\Entity\AutomobileModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AutomobileModel>
 *
 * @method AutomobileModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method AutomobileModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method AutomobileModel[]    findAll()
 * @method AutomobileModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AutomobileModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AutomobileModel::class);
    }

    public function save(AutomobileModel $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AutomobileModel $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return AutomobileModel[] Returns an array of AutomobileModel objects
     */
    public function findModelByLabel($value): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('MATCH_AGAINST(a.label) AGAINST(:val boolean) > 0 ')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }

        /**
     * @return AutomobileModel[] Returns an array of AutomobileModel objects
     */
    public function findModelByLabelLike($value): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('LOWER(a.label) LIKE CONCAT(LOWER(:val), \'%\')')
            ->orderBy('LENGTH(a.label)', 'DESC')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AutomobileModel[] Returns an array of AutomobileModel objects
     */
    public function findModelByLabelLikeWithoutSpace($value): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('REPLACE(LOWER(a.label), \' \', \'\') LIKE CONCAT(LOWER(:val),\'%\')')
            ->orderBy('LENGTH(a.label)', 'DESC')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return AutomobileModel[] Returns an array of AutomobileModel objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?AutomobileModel
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
