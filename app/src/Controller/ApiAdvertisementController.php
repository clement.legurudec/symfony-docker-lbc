<?php

namespace App\Controller;

use AddressInfo;
use App\Entity\Advertisement;
use App\Entity\AdvertisementWork;
use App\Service\AutomobileService;
use App\Entity\AdvertisementAutomobile;
use App\Entity\AdvertisementRealEstate;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\AdvertisementRepository;
use App\Repository\AutomobileModelRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\AdvertisementAutomobileRepository;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Exception\PartialDenormalizationException;

class ApiAdvertisementController extends AbstractController
{
    // Index route
    #[Route('/api/advertisement', name: 'api_advertisement_index', methods: ["GET"])]
    public function index(
        Request $request,
        AdvertisementRepository $advertisementRepository,
        SerializerInterface $serializer
    ): JsonResponse {

        // Return a list of all advertisement
        return $this->json($advertisementRepository->findAll(), 200, [], ['groups' => 'advertisement:read']);
    }

    // Read route
    #[Route('/api/advertisement/{id}', name: 'api_advertisement_read', methods: ["GET"])]
    public function read(
        $id,
        AdvertisementRepository $advertisementRepository,
        SerializerInterface $serializer
    ): JsonResponse {
        // Return an entry of advertisement
        $advertisement = $advertisementRepository->findOneBy(['id' => $id]);
        if ($advertisement) {
            return $this->json($advertisement, 200, [], ['groups' => 'advertisement:read']);
        } else {
            return $this->json([
                'status' => 400,
                'message' => 'No matching advertisement found'
            ], 400);
        }
    }

    // Create route
    #[Route('/api/advertisement', name: 'api_advertisement_create', methods: ["POST"])]
    public function create(
        Request $request,
        SerializerInterface $serializer,
        AutomobileService $automobileService,
        EntityManagerInterface $em
    ) {
        $jsonReceived = $request->getContent();
        $jsonObject = json_decode($jsonReceived);


        // In case of Automobile
        if (isset($jsonObject->type) && $jsonObject->type == Advertisement::TYPE_AUTOMOBILE) {
            if (!isset($jsonObject->model)) {
                return $this->json([
                    'status' => 400,
                    'message' => 'Missing model field in json'
                ], 400);
            }
            // Search car model for this user input
            $automobileModel = $automobileService->searchModel($jsonObject->model);

            if ($automobileModel) {
                try {
                    $advertisement = $serializer->deserialize($jsonReceived, Advertisement::class, 'json');
                    $advertisement->setModel($automobileModel);

                    $em->persist($advertisement);
                    $em->flush();
                    // Return created object
                    return $this->json($advertisement, 201, [], ['groups' => 'advertisement:read']);
                } catch (NotEncodableValueException $e) {
                    return $this->json([
                        'status' => 400,
                        'message' => $e->getMessage()
                    ], 400);
                }
            } else {
                return $this->json([
                    'status' => 400,
                    'message' => 'No matching model found'
                ], 400);
            }
            // In case of realestate
        } else if (isset($jsonObject->type) && $jsonObject->type == Advertisement::TYPE_REALESTATE) {
            // Specific code for realestate to add here
            try {
                $advertisement = $serializer->deserialize($jsonReceived, Advertisement::class, 'json');

                $em->persist($advertisement);
                $em->flush();
                return $this->json($advertisement, 201, [], ['groups' => 'advertisement:read']);
            } catch (NotEncodableValueException $e) {
                return $this->json([
                    'status' => 400,
                    'message' => $e->getMessage()
                ], 400);
            }
            // In case of work
        } else if (isset($jsonObject->type) && $jsonObject->type == Advertisement::TYPE_WORK) {
            // Specific code for work to add here
            try {
                $advertisement = $serializer->deserialize($jsonReceived, Advertisement::class, 'json');

                $em->persist($advertisement);
                $em->flush();
                return $this->json($advertisement, 201, [], ['groups' => 'advertisement:read']);
            } catch (NotEncodableValueException $e) {
                return $this->json([
                    'status' => 400,
                    'message' => $e->getMessage()
                ], 400);
            }
        }
    }

    // Update route
    #[Route('/api/advertisement/{id}', name: 'api_advertisement_update', methods: ["PUT", "PATCH"])]
    public function update(
        $id,
        Request $request,
        SerializerInterface $serializer,
        AutomobileService $automobileService,
        AdvertisementRepository $advertisementRepository,
        EntityManagerInterface $em
    ) {
        $advertisement = $advertisementRepository->findOneBy(['id' => $id]);
        // Error response when no advertisement to update found
        if (empty($advertisement)) {
            return $this->json([
                'status' => 400,
                'message' => 'No matching advertisement found'
            ], 400);
        }

        $jsonReceived = $request->getContent();
        $jsonObject = json_decode($jsonReceived);

        // In case of Automobile
        if (isset($jsonObject->type) && $jsonObject->type == Advertisement::TYPE_AUTOMOBILE) {
            if (!isset($jsonObject->model)) {
                return $this->json([
                    'status' => 400,
                    'message' => 'Missing model field in json'
                ], 400);
            }
            // Search car model for this user input
            $automobileModel = $automobileService->searchModel($jsonObject->model);

            if ($automobileModel) {
                try {
                    $serializer->deserialize($jsonReceived, Advertisement::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $advertisement]);
                    $advertisement->setModel($automobileModel);

                    $em->persist($advertisement);
                    $em->flush();
                    // Return updated object
                    return $this->json($advertisement, 201, [], ['groups' => 'advertisement:read']);
                } catch (NotEncodableValueException $e) {
                    return $this->json([
                        'status' => 400,
                        'message' => $e->getMessage()
                    ], 400);
                }
            } else {
                return $this->json([
                    'status' => 400,
                    'message' => 'No matching model found'
                ], 400);
            }
            // In case of realestate
        } else if (isset($jsonObject->type) && $jsonObject->type == Advertisement::TYPE_REALESTATE) {
            // Specific code for realestate to add here
            try {
                $advertisement = $serializer->deserialize($jsonReceived, Advertisement::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $advertisement]);

                $em->persist($advertisement);
                $em->flush();
                return $this->json($advertisement, 201, [], ['groups' => 'advertisement:read']);
            } catch (NotEncodableValueException $e) {
                return $this->json([
                    'status' => 400,
                    'message' => $e->getMessage()
                ], 400);
            }
            // In case of work
        } else if (isset($jsonObject->type) && $jsonObject->type == Advertisement::TYPE_WORK) {
            // Specific code for work to add here
            try {
                $advertisement = $serializer->deserialize($jsonReceived, Advertisement::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $advertisement]);

                $em->persist($advertisement);
                $em->flush();
                return $this->json($advertisement, 201, [], ['groups' => 'advertisement:read']);
            } catch (NotEncodableValueException $e) {
                return $this->json([
                    'status' => 400,
                    'message' => $e->getMessage()
                ], 400);
            }
        } else {
            return $this->json([
                'status' => 400,
                'message' => 'No type specified'
            ], 400);
        }
    }

    // Delete
    #[Route('/api/advertisement/{id}', name: 'api_advertisement_delete', methods: ["DELETE"])]
    public function delete(
        $id,
        AdvertisementRepository $advertisementRepository,
        EntityManagerInterface $em
    ) {
        $advertisement = $advertisementRepository->findOneBy(['id' => $id]);

        if ($advertisement) {
            $em->remove($advertisement);
            $em->flush();

            return $this->json([
                'status' => 200,
                'message' => 'Advertisement removed successfully'
            ], 200);
        } else {
            // Error message when no advertisement to remove
            return $this->json([
                'status' => 400,
                'message' => 'No advertisement with this id to remove'
            ], 400);
        }
    }
}
