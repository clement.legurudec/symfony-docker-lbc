<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\AdvertisementAutomobile;
use Doctrine\Common\Collections\Collection;
use App\Repository\AutomobileModelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AutomobileModelRepository::class)]
class AutomobileModel
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups("advertisement:read")]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'model', targetEntity: AdvertisementAutomobile::class)]
    private Collection $advertisementAutomobiles;

    #[ORM\ManyToOne(inversedBy: 'automobileModels')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups("advertisement:read")]
    private ?AutomobileBrand $brand = null;

    public function __construct()
    {
        $this->advertisementAutomobiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, AdvertisementAutomobile>
     */
    public function getAdvertisementAutomobiles(): Collection
    {
        return $this->advertisementAutomobiles;
    }

    public function addAdvertisementAutomobile(AdvertisementAutomobile $advertisementAutomobile): self
    {
        if (!$this->advertisementAutomobiles->contains($advertisementAutomobile)) {
            $this->advertisementAutomobiles->add($advertisementAutomobile);
            $advertisementAutomobile->setModel($this);
        }

        return $this;
    }

    public function removeAdvertisementAutomobile(AdvertisementAutomobile $advertisementAutomobile): self
    {
        if ($this->advertisementAutomobiles->removeElement($advertisementAutomobile)) {
            // set the owning side to null (unless already changed)
            if ($advertisementAutomobile->getModel() === $this) {
                $advertisementAutomobile->setModel(null);
            }
        }

        return $this;
    }

    public function getBrand(): ?AutomobileBrand
    {
        return $this->brand;
    }

    public function setBrand(?AutomobileBrand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }
}
