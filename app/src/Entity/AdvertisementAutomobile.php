<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\AdvertisementAutomobileRepository;

#[ORM\Entity(repositoryClass: AdvertisementAutomobileRepository::class)]
class AdvertisementAutomobile extends Advertisement
{
    #[ORM\ManyToOne(inversedBy: 'advertisementAutomobiles')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups("advertisement:read")]
    private ?AutomobileModel $model = null;

    public function getModel(): ?AutomobileModel
    {
        return $this->model;
    }

    public function setModel(?AutomobileModel $model): self
    {
        $this->model = $model;

        return $this;
    }
}
