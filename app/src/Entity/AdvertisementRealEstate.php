<?php

namespace App\Entity;

use App\Repository\AdvertisementRealEstateRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdvertisementRealEstateRepository::class)]
class AdvertisementRealEstate extends Advertisement
{
}
