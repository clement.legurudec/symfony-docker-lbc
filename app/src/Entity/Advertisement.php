<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\AdvertisementWork;
use App\Entity\AdvertisementAutomobile;
use App\Entity\AdvertisementRealEstate;
use Doctrine\ORM\Mapping\InheritanceType;
use App\Repository\AdvertisementRepository;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;

#[ORM\Entity(repositoryClass: AdvertisementRepository::class)]
#[InheritanceType('JOINED')]
#[DiscriminatorColumn(name: 'type', type: 'string')]
#[DiscriminatorMap(typeProperty: 'type',
    mapping:[
    Advertisement::TYPE_AUTOMOBILE => AdvertisementAutomobile::class,
    Advertisement::TYPE_REALESTATE => AdvertisementRealEstate::class,
    Advertisement::TYPE_WORK => AdvertisementWork::class
])]
abstract class Advertisement
{
    const TYPE_AUTOMOBILE = 'automobile';
    const TYPE_REALESTATE = 'realestate';
    const TYPE_WORK = 'work';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups("advertisement:read")]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups("advertisement:read")]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    #[Groups("advertisement:read")]
    private ?string $content = null;


    public function getEntityType()
    {
        $myDiscriminator = null;
        switch (get_class($this)) {
            case AdvertisementAutomobile::class:
                $myDiscriminator = self::TYPE_AUTOMOBILE;
                break;
            case AdvertisementRealEstate::class:
                $myDiscriminator = self::TYPE_REALESTATE;
                break;
            case AdvertisementWork::class:
                $myDiscriminator = self::TYPE_WORK;
                break;
        }
        return $myDiscriminator;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
