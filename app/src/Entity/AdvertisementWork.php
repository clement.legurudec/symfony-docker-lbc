<?php

namespace App\Entity;

use App\Repository\AdvertisementWorkRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdvertisementWorkRepository::class)]
class AdvertisementWork extends Advertisement
{
}
