<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use App\Repository\AutomobileBrandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AutomobileBrandRepository::class)]
class AutomobileBrand
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups("advertisement:read")]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: AutomobileModel::class)]
    private Collection $automobileModels;

    public function __construct()
    {
        $this->automobileModels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, AutomobileModel>
     */
    public function getAutomobileModels(): Collection
    {
        return $this->automobileModels;
    }

    public function addAutomobileModel(AutomobileModel $automobileModel): self
    {
        if (!$this->automobileModels->contains($automobileModel)) {
            $this->automobileModels->add($automobileModel);
            $automobileModel->setBrand($this);
        }

        return $this;
    }

    public function removeAutomobileModel(AutomobileModel $automobileModel): self
    {
        if ($this->automobileModels->removeElement($automobileModel)) {
            // set the owning side to null (unless already changed)
            if ($automobileModel->getBrand() === $this) {
                $automobileModel->setBrand(null);
            }
        }

        return $this;
    }
}
