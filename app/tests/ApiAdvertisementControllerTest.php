<?php

namespace App\Test;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiAdvertisementControllerTest extends WebTestCase
{

    public function testApiIndex()
    {
        $client = static::createClient();
        $client->request('GET', '/api/advertisement');
        $this->assertResponseStatusCodeSame(200);
    }

    public function testApiRead()
    {
        $client = static::createClient();
        $client->jsonRequest(
            'POST',
            '/api/advertisement',
            [
                'title' => 'Voiture 1',
                'content' => 'Lorem ipsum dolor...',
                'model' => 'rs4',
                'type' => 'automobile'
            ]
        );
        $response = $client->getResponse();
        $responseData = json_decode($response->getContent(), true);

        $client->request('GET', '/api/advertisement/' . $responseData['id']);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testApiCreate()
    {
        $client = static::createClient();
        $client->jsonRequest(
            'POST',
            '/api/advertisement',
            [
                'title' => 'Voiture 1',
                'content' => 'Lorem ipsum dolor...',
                'model' => 'rs4',
                'type' => 'automobile'
            ]
        );
        $this->assertResponseStatusCodeSame(201);
    }

    public function testApiUpdate()
    {
        $client = static::createClient();
        $client->jsonRequest(
            'POST',
            '/api/advertisement',
            [
                'title' => 'Voiture 1',
                'content' => 'Lorem ipsum dolor...',
                'model' => 'rs4',
                'type' => 'automobile'
            ]
        );
        $response = $client->getResponse();
        $responseData = json_decode($response->getContent(), true);

        $client->jsonRequest(
            'PUT',
            '/api/advertisement/'. $responseData['id'],
            [
                'title' => 'Voiture 1',
                'content' => 'Lorem ipsum dolour...',
                'model' => 'rs4',
                'type' => 'automobile'
            ]
        );
        $this->assertResponseStatusCodeSame(201);
    }

    public function testApiDelete()
    {
        $client = static::createClient();
        $client->jsonRequest(
            'POST',
            '/api/advertisement',
            [
                'title' => 'Voiture 1',
                'content' => 'Lorem ipsum dolor...',
                'model' => 'rs4',
                'type' => 'automobile'
            ]
        );
        $response = $client->getResponse();
        $responseData = json_decode($response->getContent(), true);

        $client->request('DELETE', '/api/advertisement/'. $responseData['id']);

        $this->assertResponseStatusCodeSame(200);
    }
}
