<?php

namespace App\Test;

use App\Service\AutomobileService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class AutomobileServiceTest extends KernelTestCase
{
    public function testSearchModel1() {
        self::bootKernel();

        // (2) use static::getContainer() to access the service container
        $container = static::getContainer();

        // (3) run some service & test the result
        $automobileService = $container->get(AutomobileService::class);

        $result = $automobileService->searchModel('rs4 avant');
        $this->assertEquals('Rs4', $result->getLabel());
        $this->assertEquals('Audi', $result->getBrand()->getLabel());
    }

    public function testSearchModel2() {
        self::bootKernel();

        // (2) use static::getContainer() to access the service container
        $container = static::getContainer();

        // (3) run some service & test the result
        $automobileService = $container->get(AutomobileService::class);

        $result = $automobileService->searchModel('Gran Turismo Série5');
        $this->assertEquals('Serie 5', $result->getLabel());
        $this->assertEquals('BMW', $result->getBrand()->getLabel());
    }

    public function testSearchModel3() {
        self::bootKernel();

        // (2) use static::getContainer() to access the service container
        $container = static::getContainer();

        // (3) run some service & test the result
        $automobileService = $container->get(AutomobileService::class);

        $result = $automobileService->searchModel('ds 3 crossback');
        $this->assertEquals('Ds3', $result->getLabel());
        $this->assertEquals('Citroen', $result->getBrand()->getLabel());
    }

    public function testSearchModel4() {
        self::bootKernel();

        // (2) use static::getContainer() to access the service container
        $container = static::getContainer();

        // (3) run some service & test the result
        $automobileService = $container->get(AutomobileService::class);

        $result = $automobileService->searchModel('CrossBack ds 3');
        $this->assertEquals('Ds3', $result->getLabel());
        $this->assertEquals('Citroen', $result->getBrand()->getLabel());
    }

    public function testSearchModel5() {
        self::bootKernel();

        // (2) use static::getContainer() to access the service container
        $container = static::getContainer();

        // (3) run some service & test the result
        $automobileService = $container->get(AutomobileService::class);

        $result = $automobileService->searchModel('CrossBack ds3');
        $this->assertEquals('Ds3', $result->getLabel());
        $this->assertEquals('Citroen', $result->getBrand()->getLabel());
    }

    public function testSearchModel6() {
        self::bootKernel();

        // (2) use static::getContainer() to access the service container
        $container = static::getContainer();

        // (3) run some service & test the result
        $automobileService = $container->get(AutomobileService::class);

        $result = $automobileService->searchModel('S4 Cabriolet');
        $this->assertEquals('S4 Cabriolet', $result->getLabel());
        $this->assertEquals('Audi', $result->getBrand()->getLabel());
    }
    
}
