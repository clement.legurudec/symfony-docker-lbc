# Installation du projet

Cloner le repository git suivant :
- git clone https://gitlab.com/clement.legurudec/symfony-docker-lbc.git

L'application fonctionnant sous un environnement docker, voici la procédure à suivre pour construire l'environnement.
Tout d'abord, se rendre dans le repertoire projet :
- cd symfony-docker-lbc
  
Lancer ensuite la commande suivante chargée de construire puis lancer les conteneurs :
- docker-compose up -d

Quatres conteneurs cohabitent maintenant :
- php
- nginx
- database (base de données mysql)
- container

L'accès web à Adminer est possible via l'URL : https://localhost:8081

Connectez-vous maintenant au conteneur php en lancant un bash à l'intérieur :
- docker exec -it php bash

Lancer composer install :
- composer install

La première étape consiste à construire la base de données :
- bin/console doctrine:database:create
  
Ensuite, cette base de données peut être mise à jour selon le schéma des entités :
- bin/console doctrine:schema:update --force

Charger les fixtures (données de marques et modèles de voiture): 
- symfony console doctrine:fixtures:load

Il s'agit de l'initialisation du projet, aux prochains changements sur la base de données, une migration devra être réalisée.

# Lancement des test unitaires

Des tests unitaires ont été créés et ont permis le développement des fonctionnalités souhaitées en TDD.
Ces tests se trouvent dans 2 fichiers :
- tests/ApiAdvertisementAutomobileControllerTest.php
- tests/AutomobileServiceTest.php

Le premier fichier est chargé de tester les routes API (Create, Read, Update, Delete).
Le second est quant à lui chargé de tester la fonction de recherche d'un modèle de voiture dans le service dédié.

Pour lancer ces tests.
Tout d'abord se connecter au conteneur php en ouvrant un bash :
- docker exec -it php bash

Ensuite, construire la base de données associée aux tests :
- bin/console doctrine:database:create --env=test

Mettre à jour le schéma de la base de données : 
- bin/console doctrine:schema:update --env=test --force

Charger les fixtures : 
- symfony console doctrine:fixtures:load --env=test

Lancer les tests :
- php bin/phpunit

# Entités

Tout d'abord, une entité Advertisement a été créée. Cette entité est une classe abstraite.
Cette entité reprend les champs communs à toutes les annonces, à savoir un titre et un contenu.

L'entité AdvertisementAutomobile hérite directement des champs de cette entité. Elle contient cependant 2 informations supplémentaires : à savoir une marque et un modèle de véhicule. Une relation existe entre la marque et le modèle de véhicule. Par conséquent, seul le champ modèle de véhicule a donc été ajouté dans cette entité.

Dans le cas des annonces dans la catégorie Immobilier ou Emploi, 2 nouvelles entités ont été crées, elles ne contiennent aucun champs supplémentaires :
- AdvertisementWork
- AdvertisementRealEstate

Pour finir :

Une entité AutomobileBrand dispose des champs relatifs à la marque d'un véhicule (à savoir juste un libéllé).
Une entité AutomobileModel dispose des champs relatifs à un modèle de véhicule (un libéllé et une relation avec une marque).

# Contrôleur API REST

Quatres routes ont été exposées pour permettre des opérations sur les entités Advertisement.
Ces endpoint utilisent la route suivante : 
"/api/advertisement" et "/api/advertisement/{id}" lorsqu'un id doit être précisé.

- Create : POST | Ajout d'une nouvelle entrée dans la table advertisement_automobile
- Update : PUT ou PATCH | Changement de la valeur des champs sur une entrée de la table
- Index : GET | Affichage de la liste des entrées de la table
- Read : GET | Affichage d'une entrée dans la table
- Delete : DELETE | Suppression d'une entrée dans la table

Le type d'annonce doit être précisé dans le json pour les routes : 
- Create
- Update
Ce type d'annonce doit être spécifié avec les valeurs suivantes :
- "automobile"
- "realestate"
- "work"
/!\ Tel que le controlleur update a été créé, il n'est pas possible de changer le type d'annonce avec cette route

Ce controlleur fait appel au service Serializer afin de :
- Sérialiser les données lorsqu'il s'agit d'apporter une réponse API au format JSON
- Désérializer les données lorsqu'il s'agit de créer un objet AdvertisementAutomobile à partir de données JSON

L'API renvoi un code HTTP 200 ou 201 en cas de succès, 400 en cas d'échec.

# Service AutomobileService

Lors de l'appel à la route Create ou Update, un modèle de voiture doit être trouvé puis associé à l'entité créée/modifiée. Cette recherche se base sur la chaine de caractère renseignée par le client dans le champ "model" du json.

Un service a donc été créée pour déporter ce code du contrôleur (l'objectif étant d'avoir des contrôleur avec le moins de code possible).
La fonction searchModel(), qui prend en paramètre une chaine de caractère, renvoie un modèle de voiture qui correspond le plus possible (selon le cahier des charges) à la saisie du client.
Cette fonction commence par obtenir tous les mots de la chaine de caractère et les stocker dans un tableau.
Ensuite, pour chaque mots de cette phrase :
Une recherche en BDD est faite pour potentiellement trouver une correspondance.
Ces recherches sont faites de 2 manières et dans l'ordre suivant :
- En conservant les espaces dans la colonne label en BDD (cas du DS4)
- En supprimant les espaces dans la colonne label en BDD (cas du Serie 5)

Si aucun résultat n'est trouvé, le même processus est réalisé pour le mot suivant... jusqu'à arriver à la fin de la phrase.
Si des modèles de voiture correspondent avec une recherche, ils sont stockés dans un tableau "models".
Si une ou plusieurs correspondances sont trouvées suite à la recherche associée au mot, une concatenation est faite avec le mot suivant dans la phrase, puis cette chaine est recherchée en BDD pour trouver des "competiteurs" aux resultats précédents. Si cette requêtes apportent de nouveaux modèles, ils remplacent les précédents.

Si aucun résultat n'est trouvé à l'issue de ces itérations, la fonction renvoie null.

Dans le cas d'une recherche matchant avec plusieurs modèles, la fonction findNearestModel() est appelée à la fin de la fonction. Cette fonction prend en paramètre le tableau de modèles de véhicule ainsi que la chaine de caractère grace à laquel ils ont été trouvés.
De cette manière, la fonction peut, via une simple comparaison de chaine de caractère, retourner le modèle qui correspond.

# Requêtes CURL

Voici les requêtes CURL permettant de tester l'API : 

## Index

curl --request GET \
  --url http://localhost:8080/api/advertisement

## Read

curl --request GET \
  --url http://localhost:8080/api/advertisement/1

## Create

curl --request POST \
  --url http://localhost:8080/api/advertisement \
  --header 'Content-Type: application/json' \
  --data '{
	"type": "automobile",
	"title": "Voiture 1",
	"content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec metus mauris, euismod in enim et, maximus pretium nisi.",
	"model": "s4 avant"
}'

## Update

curl --request PUT \
  --url http://localhost:8080/api/advertisement/12 \
  --header 'Content-Type: application/json' \
  --data '{
	"title": "Voiture 2",
	"content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec metus mauris, euismod in enim et, maximus pretium nisi.",
	"model": "S4 cabriolet",
	"type": "automobile"
}'

## Delete

curl --request DELETE \
  --url http://localhost:8080/api/advertisement/1
